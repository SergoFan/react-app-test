import React, { Component } from 'react';
import './index.css';

class Button extends Component {
    render() {
        return (
            <button className={"Button"} onClick={this.props.onClick}>{this.props.title}</button>
        );
    }
}

export default Button;