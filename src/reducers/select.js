import { 
    TOGGLE_SELECT_CONDITIONS,
    TOGGLE_SELECT_NOTIFICATIONS,
    TOGGLE_RADIO,
    ASSIGN_TEMPLATE,
} from '../actions/mutation-types';

const initialState = {
    conditions: 'no',
    notifications: 'no',
}

const select = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_SELECT_CONDITIONS:
            return {
                ...state,
                conditions: action.payload,
            };
        case TOGGLE_SELECT_NOTIFICATIONS:
            return {
                ...state,
                notifications: action.payload,
            };
        case TOGGLE_RADIO:
            switch (action.payload) {
                case 'conditions':
                    return {
                        ...state,
                        conditions: 'yes',
                        notifications: 'no',
                    };
                case 'notifications':
                    return {
                        ...state,
                        conditions: 'no',
                        notifications: 'yes',
                    };
                case 'both':
                    return {
                        ...state,
                        conditions: 'yes',
                        notifications: 'yes',
                    };
                default:
                    return {
                        ...state,
                    }
            }
        case ASSIGN_TEMPLATE:
            return {
                ...state,
                conditions: initialState.conditions,
                notifications: initialState.notifications,
            };
        default:
            return state;
    }
}

export default select;